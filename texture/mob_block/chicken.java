// Made with Blockbench 4.2.5
// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
// Paste this class into your mod and generate all required imports


public class chicken extends EntityModel<Entity> {
	private final ModelRenderer RightLeg;
	private final ModelRenderer LeftLeg;

	public chicken() {
		textureWidth = 128;
		textureHeight = 128;

		RightLeg = new ModelRenderer(this);
		RightLeg.setRotationPoint(-1.9F, 12.0F, 0.0F);
		RightLeg.setTextureOffset(48, 0).addBox(-2.6F, 6.0F, -2.5F, 5.0F, 6.0F, 5.0F, 0.0F, false);
		RightLeg.setTextureOffset(24, 0).addBox(-2.6F, 8.25F, -3.5F, 5.0F, 2.0F, 1.0F, 0.0F, false);
		RightLeg.setTextureOffset(0, 0).addBox(-1.1F, 10.25F, -3.2F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		LeftLeg = new ModelRenderer(this);
		LeftLeg.setRotationPoint(1.9F, 12.0F, 0.0F);
		LeftLeg.setTextureOffset(0, 0).addBox(-0.9F, 10.25F, -3.2F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		LeftLeg.setTextureOffset(24, 0).addBox(-2.4F, 8.25F, -3.5F, 5.0F, 2.0F, 1.0F, 0.0F, false);
		LeftLeg.setTextureOffset(48, 0).addBox(-2.4F, 6.0F, -2.5F, 5.0F, 6.0F, 5.0F, 0.0F, false);
	}

	@Override
	public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
		//previously the render function, render code was moved to a method below
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		RightLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		LeftLeg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}