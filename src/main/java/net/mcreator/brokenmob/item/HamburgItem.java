
package net.mcreator.brokenmob.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.Rarity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import net.minecraft.item.Food;
import net.minecraft.block.BlockState;

import net.mcreator.brokenmob.itemgroup.TabItemGroup;
import net.mcreator.brokenmob.BrokenMobModElements;

@BrokenMobModElements.ModElement.Tag
public class HamburgItem extends BrokenMobModElements.ModElement {
	@ObjectHolder("broken_mob:hamburg")
	public static final Item block = null;

	public HamburgItem(BrokenMobModElements instance) {
		super(instance, 23);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new ItemCustom());
	}

	public static class ItemCustom extends Item {
		public ItemCustom() {
			super(new Item.Properties().group(TabItemGroup.tab).maxStackSize(64).rarity(Rarity.COMMON)
					.food((new Food.Builder()).hunger(4).saturation(0.3f)

							.meat().build()));
			setRegistryName("hamburg");
		}

		@Override
		public int getItemEnchantability() {
			return 0;
		}

		@Override
		public int getUseDuration(ItemStack itemstack) {
			return 32;
		}

		@Override
		public float getDestroySpeed(ItemStack par1ItemStack, BlockState par2Block) {
			return 1F;
		}
	}
}
