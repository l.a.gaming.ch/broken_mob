package net.mcreator.brokenmob.procedures;

import net.minecraft.entity.Entity;

import net.mcreator.brokenmob.BrokenMobModVariables;
import net.mcreator.brokenmob.BrokenMobMod;

import java.util.Map;

public class FlykeykigaLisaretatokiProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				BrokenMobMod.LOGGER.warn("Failed to load dependency entity for procedure FlykeykigaLisaretatoki!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		{
			boolean _setval = (false);
			entity.getCapability(BrokenMobModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
				capability.fly = _setval;
				capability.syncPlayerVariables(entity);
			});
		}
	}
}
