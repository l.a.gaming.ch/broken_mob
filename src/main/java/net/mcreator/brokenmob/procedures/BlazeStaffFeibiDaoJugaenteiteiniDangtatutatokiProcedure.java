package net.mcreator.brokenmob.procedures;

import net.minecraft.entity.Entity;

import net.mcreator.brokenmob.BrokenMobMod;

import java.util.Map;

public class BlazeStaffFeibiDaoJugaenteiteiniDangtatutatokiProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				BrokenMobMod.LOGGER.warn("Failed to load dependency entity for procedure BlazeStaffFeibiDaoJugaenteiteiniDangtatutatoki!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		entity.setFire((int) 15);
	}
}
