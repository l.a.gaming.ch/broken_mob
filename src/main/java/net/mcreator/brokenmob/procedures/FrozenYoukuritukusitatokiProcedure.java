package net.mcreator.brokenmob.procedures;

import net.minecraft.item.ItemStack;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.brokenmob.item.BrokenitemItem;
import net.mcreator.brokenmob.BrokenMobMod;

import java.util.Random;
import java.util.Map;

public class FrozenYoukuritukusitatokiProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				BrokenMobMod.LOGGER.warn("Failed to load dependency entity for procedure FrozenYoukuritukusitatoki!");
			return;
		}
		if (dependencies.get("itemstack") == null) {
			if (!dependencies.containsKey("itemstack"))
				BrokenMobMod.LOGGER.warn("Failed to load dependency itemstack for procedure FrozenYoukuritukusitatoki!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		ItemStack itemstack = (ItemStack) dependencies.get("itemstack");
		if (entity instanceof LivingEntity) {
			LivingEntity _ent = (LivingEntity) entity;
			if (!_ent.world.isRemote()) {
				BrokenitemItem.shoot(_ent.world, _ent, new Random(), 2, 0, 0);
			}
		}
		{
			ItemStack _ist = itemstack;
			if (_ist.attemptDamageItem((int) 1, new Random(), null)) {
				_ist.shrink(1);
				_ist.setDamage(0);
			}
		}
	}
}
