package net.mcreator.brokenmob.procedures;

import net.minecraft.world.IWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.entity.passive.PigEntity;
import net.minecraft.entity.passive.IronGolemEntity;
import net.minecraft.entity.passive.CowEntity;
import net.minecraft.entity.passive.ChickenEntity;
import net.minecraft.entity.monster.ZombieEntity;
import net.minecraft.entity.monster.WitherSkeletonEntity;
import net.minecraft.entity.monster.SpiderEntity;
import net.minecraft.entity.monster.SkeletonEntity;
import net.minecraft.entity.monster.CreeperEntity;
import net.minecraft.entity.monster.BlazeEntity;
import net.minecraft.entity.Entity;

import net.mcreator.brokenmob.block.ZombieBlockBlock;
import net.mcreator.brokenmob.block.WitherBlockBlock;
import net.mcreator.brokenmob.block.SpiderBlockBlock;
import net.mcreator.brokenmob.block.SkeletonBlockBlock;
import net.mcreator.brokenmob.block.PigBlockBlock;
import net.mcreator.brokenmob.block.GolemBlockBlock;
import net.mcreator.brokenmob.block.CreeperBlockBlock;
import net.mcreator.brokenmob.block.CowBlockBlock;
import net.mcreator.brokenmob.block.ChickenBlockBlock;
import net.mcreator.brokenmob.block.BlazeBlockBlock;
import net.mcreator.brokenmob.BrokenMobMod;

import java.util.Map;

public class BrokenitemFeibiDaoJugaenteiteiniDangtatutatokiProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				BrokenMobMod.LOGGER.warn("Failed to load dependency world for procedure BrokenitemFeibiDaoJugaenteiteiniDangtatutatoki!");
			return;
		}
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				BrokenMobMod.LOGGER.warn("Failed to load dependency entity for procedure BrokenitemFeibiDaoJugaenteiteiniDangtatutatoki!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		Entity entity = (Entity) dependencies.get("entity");
		if (entity instanceof CreeperEntity) {
			if (!entity.world.isRemote())
				entity.remove();
			world.setBlockState(new BlockPos(entity.getPosX(), entity.getPosY(), entity.getPosZ()), CreeperBlockBlock.block.getDefaultState(), 3);
		} else if (entity instanceof ZombieEntity) {
			if (!entity.world.isRemote())
				entity.remove();
			world.setBlockState(new BlockPos(entity.getPosX(), entity.getPosY(), entity.getPosZ()), ZombieBlockBlock.block.getDefaultState(), 3);
		} else if (entity instanceof SkeletonEntity) {
			if (!entity.world.isRemote())
				entity.remove();
			world.setBlockState(new BlockPos(entity.getPosX(), entity.getPosY(), entity.getPosZ()), SkeletonBlockBlock.block.getDefaultState(), 3);
		} else if (entity instanceof ChickenEntity) {
			if (!entity.world.isRemote())
				entity.remove();
			world.setBlockState(new BlockPos(entity.getPosX(), entity.getPosY(), entity.getPosZ()), ChickenBlockBlock.block.getDefaultState(), 3);
		} else if (entity instanceof PigEntity) {
			if (!entity.world.isRemote())
				entity.remove();
			world.setBlockState(new BlockPos(entity.getPosX(), entity.getPosY(), entity.getPosZ()), PigBlockBlock.block.getDefaultState(), 3);
		} else if (entity instanceof CowEntity) {
			if (!entity.world.isRemote())
				entity.remove();
			world.setBlockState(new BlockPos(entity.getPosX(), entity.getPosY(), entity.getPosZ()), CowBlockBlock.block.getDefaultState(), 3);
		} else if (entity instanceof SpiderEntity) {
			if (!entity.world.isRemote())
				entity.remove();
			world.setBlockState(new BlockPos(entity.getPosX(), entity.getPosY(), entity.getPosZ()), SpiderBlockBlock.block.getDefaultState(), 3);
		} else if (entity instanceof BlazeEntity) {
			if (!entity.world.isRemote())
				entity.remove();
			world.setBlockState(new BlockPos(entity.getPosX(), entity.getPosY(), entity.getPosZ()), BlazeBlockBlock.block.getDefaultState(), 3);
		} else if (entity instanceof WitherSkeletonEntity) {
			if (!entity.world.isRemote())
				entity.remove();
			world.setBlockState(new BlockPos(entity.getPosX(), entity.getPosY(), entity.getPosZ()), WitherBlockBlock.block.getDefaultState(), 3);
		} else if (entity instanceof IronGolemEntity) {
			if (!entity.world.isRemote())
				entity.remove();
			world.setBlockState(new BlockPos(entity.getPosX(), entity.getPosY(), entity.getPosZ()), GolemBlockBlock.block.getDefaultState(), 3);
		}
	}
}
