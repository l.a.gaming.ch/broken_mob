package net.mcreator.brokenmob.procedures;

import net.minecraft.item.ItemStack;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import net.mcreator.brokenmob.item.WitherHeadItem;
import net.mcreator.brokenmob.BrokenMobModVariables;
import net.mcreator.brokenmob.BrokenMobMod;

import java.util.Map;

public class FlykeykigaYasaretatokiProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				BrokenMobMod.LOGGER.warn("Failed to load dependency entity for procedure FlykeykigaYasaretatoki!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (((entity instanceof LivingEntity) ? ((LivingEntity) entity).getItemStackFromSlot(EquipmentSlotType.HEAD) : ItemStack.EMPTY)
				.getItem() == WitherHeadItem.helmet) {
			{
				boolean _setval = (true);
				entity.getCapability(BrokenMobModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
					capability.fly = _setval;
					capability.syncPlayerVariables(entity);
				});
			}
		}
	}
}
